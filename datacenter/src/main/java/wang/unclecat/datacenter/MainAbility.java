package wang.unclecat.datacenter;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import wang.unclecat.datacenter.db.Alarm;
import wang.unclecat.datacenter.db.AlarmsDBOperation;
import wang.unclecat.mylibrary.MyLog;

import java.util.List;

/**
 * 测试数据库crud
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_insert).setClickedListener(component -> {
            AlarmsDBOperation.insert(this);
        });

        findComponentById(ResourceTable.Id_query).setClickedListener(component -> {
            Alarm alarm = AlarmsDBOperation.queryFirst(this);
            if (alarm!=null) {
                MyLog.info(alarm.toString());

            }
        });

        findComponentById(ResourceTable.Id_queryAll).setClickedListener(component -> {
            List<Alarm> list = AlarmsDBOperation.queryAll(this);
            if (list!=null) {
                for (Alarm alarm : list) {
                    MyLog.info(alarm.toString());
                }
            }
        });
    }
}
