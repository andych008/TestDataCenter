package wang.unclecat.mylibrary;

import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;
import ohos.global.resource.Entry;
import ohos.global.resource.RawFileEntry;
import ohos.mp.metadata.binding.MetaDataFramework;
import ohos.mp.metadata.binding.common.Log;
import ohos.mp.metadata.binding.uioperate.UiOperatorManager;

import java.io.IOException;
import java.lang.reflect.Method;

public class MetaDataFrameworkExtra {

    public static void init(AbilityPackage var0) {
        MetaDataFramework.appContext = var0;
        loadAllMetaDataSchema(var0);//fix: SchemaParseUtil.loadAllMetaDataSchema(var0);
        UiOperatorManager.init();
    }

    //fix: SchemaParseUtil.loadAllMetaDataSchema(var0);
    public static void loadAllMetaDataSchema(Context var0) {
        String moduleName = var0.getHapModuleInfo().getModuleName();

        try {
            RawFileEntry var1 = var0.getResourceManager().getRawFileEntry(moduleName+"/resources/rawfile/jsonschema/");
            Entry[] var2 = var1.getEntries();

            for(int var3 = 0; var3 < var2.length; ++var3) {
                String var4 = var2[var3].getPath();
                String var5 = moduleName+"/resources/rawfile/jsonschema/" + var4;
                loadSingleMetadataSchema(var0, var5);
            }
        } catch (IOException var6) {
            Log.error("Load All MetaData Schema Failed: IO exception");
        }

    }

    //call the method using reflection
    private static void loadSingleMetadataSchema(Context var0, String var1){
        try {
            Class<?> threadClazz = Class.forName("ohos.mp.metadata.binding.schema.SchemaParseUtil");
            Method method = threadClazz.getDeclaredMethod("loadSingleMetadataSchema", Context.class, String.class);
            method.setAccessible(true);
            System.out.println(method.invoke(null, var0, var1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}